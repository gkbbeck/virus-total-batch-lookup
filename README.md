# virus-total-batch-lookup

Simple Python test of the VT API to lookup a list of IPs on Virus Total.

## Installation

### Dependencies

Install Python 3, requests, and dotenv

`pip install requests dotenv`

Or, on Void Linux,

`xbps-install python3-requests python3-dotenv`

## Usage

Obtain your VT API key and in the directory with the script create a file .env with 

`api_key={key}`

Then you can pass the path to your newline-separated list of IPs to lookup. If run without it'll fall back to ips.csv located in the script's working directory.

The console window will print out brief reports on the IP, and a file, output.txt, will give you in-depth information.
