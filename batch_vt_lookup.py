#!/usr/bin/env python
"""This module provides a class and functions to lookup IPs and URLs on VirusTotal"""

# Obtain API key from https://virustotal.com/gui/user/{username}/apikey

# Licensed under GNU AGPL v3 or later

import time

# import json
import pathlib
import os
import sys
import ipaddress
import base64

import logging

try:
    import requests
except ModuleNotFoundError:
    print("Install requests module before running!")
    sys.exit()
try:
    from dotenv import load_dotenv
except ModuleNotFoundError:
    print("Install dotenv module before running!")
    sys.exit()

load_dotenv()

# URL to send requests to

if len(sys.argv) != 2:
    print("Specify a file to read IPs and URLs from!")
    sys.exit(1)

vt_lookup_config = {
    "in_file": sys.argv[1],
    "out_file": pathlib.Path.cwd() / "output.txt",
    "delay": 16,
    "headers": {"Accept": "application/json", "x-apikey": os.getenv("api_key")},
}


logging.basicConfig(
    level=logging.DEBUG,
    filename="vt.log",
    filemode="w",
    format="%(levelname)s - %(message)s",
)


class Query:
    """
    Contains the base class to be looked up on VT

    Attributes:
       line (string): The value to be queried
       request (requests.Response): the response object VT returns us
       base_url (string): the URL from which specific ones are derived
       ip_url (string): the URL for looking up specific IPs
       dom_url (string): the URL for looking up domains

    """

    def __init__(self, line):
        """
        Inits the class attributes

        Args:
            line (string):
        """

        self.line = line
        self.request = requests.Response()
        self.base_url = "https://www.virustotal.com/api/v3/"
        self.ip_url = f"{self.base_url}ip_addresses/"
        self.dom_url = f"{self.base_url}urls/"

    def check_for_errors(self):
        """Checks if the API reqest returns an error, very poorly"""
        match self.request.status_code:
            case 200:
                return 0
            case _:
                return 1


class IPQuery(Query):
    def lookup(self):
        self.request = requests.get(
            f"{self.ip_url}{self.line}", headers=vt_lookup_config["headers"]
        )

        return self.check_for_errors()


class URLQuery(Query):
    def __init__(self, line):
        super().__init__(line)

        self.encoded_url = (
            # VT uses base64 encodings without the padding of =
            base64.urlsafe_b64encode(self.line.encode())
            .decode()
            .strip("=")
        )

    def lookup(self):
        """Looks up the URL"""
        self.request = requests.get(
            f"{self.dom_url}{self.encoded_url}", headers=vt_lookup_config["headers"]
        )
        error = self.check_for_errors()
        return error


with open(vt_lookup_config["in_file"], "r", encoding="utf-8") as in_fp:
    with open(vt_lookup_config["out_file"], "a", encoding="utf-8") as out_fp:
        for ip in in_fp:
            # lineip the whitespace from the IPs
            ip = ip.strip("\n\r ")
            try:
                ipaddress.ip_address(ip)
            except ValueError:  # not the most elegant solution but hey
                ip = URLQuery(ip)
            else:
                ip = IPQuery(ip)

            print(
                f'Delaying {str(vt_lookup_config["delay"])} seconds before looking up {ip.line}'
            )
            time.sleep(vt_lookup_config["delay"])
            if ip.lookup() == 1:
                print(f"Skipping {ip.line} due to API error: {ip.request.status_code}")
                json_output = ip.request.json()
                error = json_output.get("error", {}).get("message")
                print(f"VT says: {str(error)}")
            else:
                # Probably should replace this print with getattr
                json_output = ip.request.json()
                short_output = (
                    json_output.get("data").get("attributes").get("last_analysis_stats")
                )
                print(f"{str(short_output)}")
                out_fp.write(f"{ip.line}\n{ip.request.json()}\n\n\n")

input("All IPs processed. Press ENTER to exit.")
